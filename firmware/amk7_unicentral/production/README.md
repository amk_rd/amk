##amk7_unicentral##
|NAZWA PLIKU 					|	DATA			| COMMIT Z KTOREGO POCHODZI KOMPILACJA		| OPIS COMMITU					|
|---|---|---|---|
| [amk7_unicentral-v83-c114-e071558-20180720.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v83-c114-e071558-20180720.bl2) | 20180720-13:02:10 | [e071558592dfdeeb7114a0e3f6ec3ee2f00ec002](https://bitbucket.org/amk_rd/amk7_unicentral/commits/e071558592dfdeeb7114a0e3f6ec3ee2f00ec002) | bitbucket-pipelines.yml edited online with Bitbucket |
| [amk7_unicentral-v84-c114-9aefdc4-20180720.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v84-c114-9aefdc4-20180720.bl2) | 20180720-13:12:25 | [9aefdc4b175ea0748a30861f35f8e7ada6cb8233](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9aefdc4b175ea0748a30861f35f8e7ada6cb8233) | bitbucket-pipelines.yml edited online with Bitbucket |
| [amk7_unicentral-v86-c114-681ce61-20180720.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v86-c114-681ce61-20180720.bl2) | 20180720-13:36:24 | [681ce611311aed149406f9ef21cc5fc2ef38ac02](https://bitbucket.org/amk_rd/amk7_unicentral/commits/681ce611311aed149406f9ef21cc5fc2ef38ac02) | bitbucket-pipelines.yml edited online with Bitbucket |
 
| [amk7_unicentral-v87-c129-ee25894-20180723.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v87-c129-ee25894-20180723.bl2) | 20180723-12:32:20 | [ee258949627a9f42b1885a0ea3624028f3064286](https://bitbucket.org/amk_rd/amk7_unicentral/commits/ee258949627a9f42b1885a0ea3624028f3064286) | Merge branch 'master' of https://bitbucket.org/amk_rd/amk7_unicentral |
 
| [amk7_unicentral-v88-c132-211bef9-20180723.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v88-c132-211bef9-20180723.bl2) | 20180723-15:16:54 | [211bef9fa81df97dc493ac40d899d2d99405a894](https://bitbucket.org/amk_rd/amk7_unicentral/commits/211bef9fa81df97dc493ac40d899d2d99405a894) | resetowaniie raspberry, reinit modbusSlave |
 
| [amk7_unicentral-v89-c163-82a57c8-20180724.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v89-c163-82a57c8-20180724.bl2) | 20180724-15:23:20 | [82a57c873f1208fbffae7193188f730f8e1f58d3](https://bitbucket.org/amk_rd/amk7_unicentral/commits/82a57c873f1208fbffae7193188f730f8e1f58d3) | dodanie alarmow na min i max temp nawiewu.Zmiana temp. procesu wedlug min max temp nawiewu. Algorytm sterowania jednostką freonowa |
 
| [amk7_unicentral-v90-c164-c84602a-20180724.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v90-c164-c84602a-20180724.bl2) | 20180724-15:36:21 | [c84602a29c5697a6d486eedde492100fa9b93ec3](https://bitbucket.org/amk_rd/amk7_unicentral/commits/c84602a29c5697a6d486eedde492100fa9b93ec3) | dodalem INFO_TEMPERATURA_ZADANA_PROCESU i zamienilem z INFO_TEMPERATURA_ZADANA_SYSTEMU poniewaz musialem miec temp ktora sterowala algorytmem nie mogla to byc temp.systemu bo w openhubie by sie zmieniala jak glupia |
 
| [amk7_unicentral-v91-c167-9b7222a-20180725.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v91-c167-9b7222a-20180725.bl2) | 20180725-08:39:28 | [9b7222ae8cb9ae4358288847126d2b5c7ebfa81f](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9b7222ae8cb9ae4358288847126d2b5c7ebfa81f) | wdt dla raspberry jezeli po 5min nie bedzie transmisji |
 
| [amk7_unicentral-v92-c171-d095df0-20180725.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v92-c171-d095df0-20180725.bl2) | 20180725-10:05:13 | [d095df0b8865ce381c75a2bfa501a8c34cf69b5d](https://bitbucket.org/amk_rd/amk7_unicentral/commits/d095df0b8865ce381c75a2bfa501a8c34cf69b5d) | naprawienie modbusa dla panelu, dodanie wdt |
 
| [amk7_unicentral-v93-c176-7b68ccc-20180725.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v93-c176-7b68ccc-20180725.bl2) | 20180725-14:57:37 | [7b68ccccbbc8e39825a8800deae7e5e87db42f04](https://bitbucket.org/amk_rd/amk7_unicentral/commits/7b68ccccbbc8e39825a8800deae7e5e87db42f04) | PID dla chlodzenia |
 
| [amk7_unicentral-v94-c179-4d6a2a5-20180726.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v94-c179-4d6a2a5-20180726.bl2) | 20180726-12:47:39 | [4d6a2a554e500c0d88cab6190240a248cf5b346c](https://bitbucket.org/amk_rd/amk7_unicentral/commits/4d6a2a554e500c0d88cab6190240a248cf5b346c) | inicjalizowanie flasha zerami |
 
| [amk7_unicentral-v95-c184-e31ca76-20180731.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v95-c184-e31ca76-20180731.bl2) | 20180731-15:18:47 | [e31ca769a6fa76cca9d38638cd91775ca41db853](https://bitbucket.org/amk_rd/amk7_unicentral/commits/e31ca769a6fa76cca9d38638cd91775ca41db853) | open colector for raspberry |
 
| [amk7_unicentral-v96-c205-2b62f16-20180803.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v96-c205-2b62f16-20180803.bl2) | 20180803-12:08:48 | [2b62f164fbc0784a558cb7eaaebee759bcd3d92a](https://bitbucket.org/amk_rd/amk7_unicentral/commits/2b62f164fbc0784a558cb7eaaebee759bcd3d92a) | dodanie sygnalizatora awari i ostrzezen |
 
| [amk7_unicentral-v97-c207-9d52e37-20180803.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v97-c207-9d52e37-20180803.bl2) | 20180803-14:54:46 | [9d52e37f5090134f0fccb498986bbaacf8d11851](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9d52e37f5090134f0fccb498986bbaacf8d11851) | DI active |
 
| [amk7_unicentral-v98-c213-b87da4a-20180806.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v98-c213-b87da4a-20180806.bl2) | 20180806-08:19:51 | [b87da4a1584677ee3e2d660d810c508b0334c84d](https://bitbucket.org/amk_rd/amk7_unicentral/commits/b87da4a1584677ee3e2d660d810c508b0334c84d) | dodalem staly sprez, nie testowalem bo nie mam jak zabardzo |
 
| [amk7_unicentral-v99-c214-9b8013e-20180806.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v99-c214-9b8013e-20180806.bl2) | 20180806-08:49:23 | [9b8013ed2399994b07f9d2dae372da21dd485ed0](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9b8013ed2399994b07f9d2dae372da21dd485ed0) | pomylone byly wejscia analogowe |
 
| [amk7_unicentral-v100-c251-b7759d8-20180808.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v100-c251-b7759d8-20180808.bl2) | 20180808-11:13:48 | [b7759d8834ddb3409f98522b69859da24fe2d0dd](https://bitbucket.org/amk_rd/amk7_unicentral/commits/b7759d8834ddb3409f98522b69859da24fe2d0dd) | e9 jako reset dla raspberry zmiana rejestro dla harmon |
 
| [amk7_unicentral-v101-c255-45cd475-20180820.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v101-c255-45cd475-20180820.bl2) | 20180820-11:59:49 | [45cd4756277f63dd2e8919981dd9e1c7142a67e3](https://bitbucket.org/amk_rd/amk7_unicentral/commits/45cd4756277f63dd2e8919981dd9e1c7142a67e3) | poprawilem komunikacje z panelem, po wczesniejszych zmianach w modbu slave cos zepsulem i nie przetestowalem |
 
| [amk7_unicentral-v102-c419-c023633-20180903.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v102-c419-c023633-20180903.bl2) | 20180903-12:56:41 | [c023633dd424f1b05b04e8f94f3fc0ebc3a3566b](https://bitbucket.org/amk_rd/amk7_unicentral/commits/c023633dd424f1b05b04e8f94f3fc0ebc3a3566b) | usuniecie z main wgrywania harmon |
 
| [amk7_unicentral-v103-c420-79ab410-20180903.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v103-c420-79ab410-20180903.bl2) | 20180903-12:58:13 | [79ab4109fd365da37cfa3b5df1c916278fb89abf](https://bitbucket.org/amk_rd/amk7_unicentral/commits/79ab4109fd365da37cfa3b5df1c916278fb89abf) | kompilacja po merge |
 
| [amk7_unicentral-v104-c432-69b4ffe-20180904.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v104-c432-69b4ffe-20180904.bl2) | 20180904-15:58:16 | [69b4ffe4092cb6385a164c7943f306a5d40a3965](https://bitbucket.org/amk_rd/amk7_unicentral/commits/69b4ffe4092cb6385a164c7943f306a5d40a3965) | dodałem konfiguracje centrali dla gwc recyrkulacji pompy ciepla i gwc |
 
| [amk7_unicentral-v106-c464-ddf7d9f-20180906.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v106-c464-ddf7d9f-20180906.bl2) | 20180906-10:36:19 | [ddf7d9f8ef02da5bc9c98a06a6e7ec2b0b22f501](https://bitbucket.org/amk_rd/amk7_unicentral/commits/ddf7d9f8ef02da5bc9c98a06a6e7ec2b0b22f501) | naprawilem blad, zostala dodana pompa ciepla gwc a teraz recyrkulacje dodaje |
 
| [amk7_unicentral-v107-c490-75cc8bf-20180907.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v107-c490-75cc8bf-20180907.bl2) | 20180907-14:26:19 | [75cc8bf57a716f7b8bf8423de7718521f7c4a470](https://bitbucket.org/amk_rd/amk7_unicentral/commits/75cc8bf57a716f7b8bf8423de7718521f7c4a470) | sterowanie wentylatorami podzieliłem na tryby teraz robie regulacje odzysk w zaleznosci od parametru zadanego |
 
| [amk7_unicentral-v108-c495-b6eac40-20180907.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v108-c495-b6eac40-20180907.bl2) | 20180907-15:18:41 | [b6eac4002e011a6199246de1d32ee5e9fe95cb9c](https://bitbucket.org/amk_rd/amk7_unicentral/commits/b6eac4002e011a6199246de1d32ee5e9fe95cb9c) | sprawnosc wymiennika liczy sie ok |
 
| [amk7_unicentral-v109-c499-f3869f5-20180910.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v109-c499-f3869f5-20180910.bl2) | 20180910-16:14:12 | [f3869f52771710d5a97cf165a21dd368de442a0c](https://bitbucket.org/amk_rd/amk7_unicentral/commits/f3869f52771710d5a97cf165a21dd368de442a0c) | naprawienie chlodnicy 2 sekcje, robie optymalizacje odzysku |
 
| [amk7_unicentral-v110-c504-ceb5bc4-20180911.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v110-c504-ceb5bc4-20180911.bl2) | 20180911-08:59:59 | [ceb5bc4d468ea1137e4089f577ad1fdb41c49b9c](https://bitbucket.org/amk_rd/amk7_unicentral/commits/ceb5bc4d468ea1137e4089f577ad1fdb41c49b9c) | przy starcie sterownika juz chyba nie pojawia sie napiecie na pwm |
 
| [amk7_unicentral-v111-c528-9ef9f06-20180911.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v111-c528-9ef9f06-20180911.bl2) | 20180911-15:36:42 | [9ef9f06156ae0d472c7f951ea83bbd0110edf4ec](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9ef9f06156ae0d472c7f951ea83bbd0110edf4ec) | dorobienie optymalizacji, udid, robie wpisywanie kodu aktywacyjnego |
 
| [amk7_unicentral-v112-c582-0fccc10-20180913.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v112-c582-0fccc10-20180913.bl2) | 20180913-07:57:49 | [0fccc10632a27fe75b6ac44e355cea16f150aafb](https://bitbucket.org/amk_rd/amk7_unicentral/commits/0fccc10632a27fe75b6ac44e355cea16f150aafb) | dodanie kodu aktywacyjnego |
 
| [amk7_unicentral-v113-c588-ea0de88-20180917.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v113-c588-ea0de88-20180917.bl2) | 20180917-13:19:08 | [ea0de887ff8d6cffce0c952ea3a74e4b638e7e90](https://bitbucket.org/amk_rd/amk7_unicentral/commits/ea0de887ff8d6cffce0c952ea3a74e4b638e7e90) | zmiana nazw zmiennych konfigurowania centrali |
 
| [amk7_unicentral-v114-c602-8ba0ee9-20180919.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v114-c602-8ba0ee9-20180919.bl2) | 20180919-12:37:19 | [8ba0ee962e6c9542ded2f29f998c773002703212](https://bitbucket.org/amk_rd/amk7_unicentral/commits/8ba0ee962e6c9542ded2f29f998c773002703212) | zmiany pod panel okragły |
 
| [amk7_unicentral-v115-c604-179b468-20180921.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v115-c604-179b468-20180921.bl2) | 20180921-11:19:53 | [179b4686d244a1c18a1b74d4fcc5e05b2dd602e9](https://bitbucket.org/amk_rd/amk7_unicentral/commits/179b4686d244a1c18a1b74d4fcc5e05b2dd602e9) | przy uruchamianiu sterownika wylaczam raspberry a dopiero po 3 sekundach wlaczam |
 
| [amk7_unicentral-v116-c605-03a1590-20180927.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v116-c605-03a1590-20180927.bl2) | 20180927-09:12:55 | [03a1590be060c0ee6d5252100dd752163bf20f83](https://bitbucket.org/amk_rd/amk7_unicentral/commits/03a1590be060c0ee6d5252100dd752163bf20f83) | kompilacja |
 
| [amk7_unicentral-v117-c734-0576746-20181002.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v117-c734-0576746-20181002.bl2) | 20181002-08:27:38 | [057674685bc8f5c42fad7911bc2087b0fe55a6fc](https://bitbucket.org/amk_rd/amk7_unicentral/commits/057674685bc8f5c42fad7911bc2087b0fe55a6fc) | merge to master |
 
| [amk7_unicentral-v118-c765-25efbc3-20181002.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v118-c765-25efbc3-20181002.bl2) | 20181002-15:26:58 | [25efbc301edc4719b95063b128ce96a90cd50a38](https://bitbucket.org/amk_rd/amk7_unicentral/commits/25efbc301edc4719b95063b128ce96a90cd50a38) | Obliczam CRC32 z UDID1-5 |
 
| [amk7_unicentral-v119-c788-44618d9-20181003.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v119-c788-44618d9-20181003.bl2) | 20181003-16:33:24 | [44618d9c9e78f74b8615ce3127c244c5779ec996](https://bitbucket.org/amk_rd/amk7_unicentral/commits/44618d9c9e78f74b8615ce3127c244c5779ec996) | zmieniłem tryb wentylacji, przepustnice wybiera sie w konfiguracji |
 
| [amk7_unicentral-v120-c810-61225ac-20181004.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v120-c810-61225ac-20181004.bl2) | 20181004-15:41:28 | [61225ac70e84979947fe0a6fe65f2aeeef4c4c01](https://bitbucket.org/amk_rd/amk7_unicentral/commits/61225ac70e84979947fe0a6fe65f2aeeef4c4c01) | otwieranie przepustnic jezeli sa i wlaczenie wentylatorow jezeli jest potwierdzenie od prrepustnic, przebudowalem awarie i sposob uruchomienia w przypadku awari |
 
| [amk7_unicentral-v121-c814-118c298-20181004.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v121-c814-118c298-20181004.bl2) | 20181004-15:59:20 | [118c298728082abf56e673ec94641716f01dbd8e](https://bitbucket.org/amk_rd/amk7_unicentral/commits/118c298728082abf56e673ec94641716f01dbd8e) | wyjatek blednej konfiguracji dla przepustnic i gwc |
 
| [amk7_unicentral-v122-c823-9f8e053-20181005.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v122-c823-9f8e053-20181005.bl2) | 20181005-12:08:37 | [9f8e05383d82446df40d0e20a95c4f17143a1c94](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9f8e05383d82446df40d0e20a95c4f17143a1c94) | dodalem wersje oprogramowania do rejestrow |
 
| [amk7_unicentral-v123-c828-fea6d62-20181005.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v123-c828-fea6d62-20181005.bl2) | 20181005-14:52:49 | [fea6d62323e93553334c67347d5c9429e2e8b04f](https://bitbucket.org/amk_rd/amk7_unicentral/commits/fea6d62323e93553334c67347d5c9429e2e8b04f) | resetowanie komunikatu o przegladzie centrali |
 
| [amk7_unicentral-v124-c831-0bfb552-20181005.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v124-c831-0bfb552-20181005.bl2) | 20181005-17:43:09 | [0bfb55269d908511fc7ce10cb5bd4beec5ae4d96](https://bitbucket.org/amk_rd/amk7_unicentral/commits/0bfb55269d908511fc7ce10cb5bd4beec5ae4d96) | oznaczenie co sie dzieje z wymiennikiem i zabezpieczenie ze jak na panelu bedzi eponizej 10 to niech ustawi temp wywiew prawdopodobnie bedzie wtedy zepsuty temp albo jego brak |
 
| [amk7_unicentral-v125-c843-9cd6f16-20181008.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v125-c843-9cd6f16-20181008.bl2) | 20181008-09:20:10 | [9cd6f16ce91ac1bd1680c0155364b9ba0060bba7](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9cd6f16ce91ac1bd1680c0155364b9ba0060bba7) | kod aktywacyjny zeruje na poczatku |
 
| [amk7_unicentral-v126-c845-091f5c1-20181008.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v126-c845-091f5c1-20181008.bl2) | 20181008-09:53:43 | [091f5c1ff8865ede80d0675f93346532a5ab4373](https://bitbucket.org/amk_rd/amk7_unicentral/commits/091f5c1ff8865ede80d0675f93346532a5ab4373) | znalazlem problem w nie programowaniu przez bootloader |
 
| [amk7_unicentral-v127-c854-1999fac-20181008.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v127-c854-1999fac-20181008.bl2) | 20181008-15:49:14 | [1999fac90ab29c735f894cd4f09d856395a6c335](https://bitbucket.org/amk_rd/amk7_unicentral/commits/1999fac90ab29c735f894cd4f09d856395a6c335) | wdt dla panelu, ostrzezenia w przypadku awari czujnika wywiewu i rozlaczonego panelu |
 
| [amk7_unicentral-v128-c873-3ae941a-20181009.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v128-c873-3ae941a-20181009.bl2) | 20181009-16:12:10 | [3ae941ab1dbe9c47c5ffbf585dc48eb00ab42fbf](https://bitbucket.org/amk_rd/amk7_unicentral/commits/3ae941ab1dbe9c47c5ffbf585dc48eb00ab42fbf) | wydaje mi si eze sterowanie nagrzewnica 3punktowa okej dodac funkcje sterowania przy wygrzewie zeby nie bylo tylko sterowanie przekaznikiem tylko caly algorytm |
 
| [amk7_unicentral-v129-c875-0983559-20181011.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v129-c875-0983559-20181011.bl2) | 20181011-08:42:13 | [0983559f060eec3fc322acdea24a3ae68134718b](https://bitbucket.org/amk_rd/amk7_unicentral/commits/0983559f060eec3fc322acdea24a3ae68134718b) | nagrzewnica zamknij otworz 3stopniowa okej |
 
| [amk7_unicentral-v130-c893-734fcea-20181011.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v130-c893-734fcea-20181011.bl2) | 20181011-15:17:02 | [734fcea2e6e188be1462736688f311a3eed463e1](https://bitbucket.org/amk_rd/amk7_unicentral/commits/734fcea2e6e188be1462736688f311a3eed463e1) | czytanie zegarka jak jest blad flasha, dodanie wymiennikow Bartosz,robie optymalizacje odzysku |
 
| [amk7_unicentral-v131-c914-3e1f941-20181012.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v131-c914-3e1f941-20181012.bl2) | 20181012-16:13:45 | [3e1f941f2c5ac3d45beda3b49fdeae3db3b8a9a6](https://bitbucket.org/amk_rd/amk7_unicentral/commits/3e1f941f2c5ac3d45beda3b49fdeae3db3b8a9a6) | optymalizacja odzysku tryb zimowy musze zrobic i przetestowac |
 
| [amk7_unicentral-v132-c937-a8d46a6-20181015.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v132-c937-a8d46a6-20181015.bl2) | 20181015-14:53:33 | [a8d46a6ebad5c03f40673cef4de46afd579b37f3](https://bitbucket.org/amk_rd/amk7_unicentral/commits/a8d46a6ebad5c03f40673cef4de46afd579b37f3) | optymalizacja odzysku, tryb zimowy |
 
| [amk7_unicentral-v133-c939-220c6d0-20181015.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v133-c939-220c6d0-20181015.bl2) | 20181015-15:59:05 | [220c6d076d6a5ab0cbad90d00e482b871e61d6bd](https://bitbucket.org/amk_rd/amk7_unicentral/commits/220c6d076d6a5ab0cbad90d00e482b871e61d6bd) | ostrzezenie o zablokowanej centrali |
 
| [amk7_unicentral-v134-c940-34ec288-20181016.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v134-c940-34ec288-20181016.bl2) | 20181016-08:51:27 | [34ec288af4245555206baa011f7852bb89839e0b](https://bitbucket.org/amk_rd/amk7_unicentral/commits/34ec288af4245555206baa011f7852bb89839e0b) | dodanie do panelu info o trybie zimowym i o przegladzie centrali |
 
| [amk7_unicentral-v135-c944-6f8e70e-20181016.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v135-c944-6f8e70e-20181016.bl2) | 20181016-11:24:39 | [6f8e70ec85f1085ef16f191c211ea4c2b35118bb](https://bitbucket.org/amk_rd/amk7_unicentral/commits/6f8e70ec85f1085ef16f191c211ea4c2b35118bb) | gdy nie ma czujnikow 2-10 to przelacza sie na wydajnosc zadana |
 
| [amk7_unicentral-v136-c945-02ebe62-20181016.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v136-c945-02ebe62-20181016.bl2) | 20181016-16:11:28 | [02ebe620869d522d9734a5942c7930f102327b48](https://bitbucket.org/amk_rd/amk7_unicentral/commits/02ebe620869d522d9734a5942c7930f102327b48) | znalazlem bug, polegajacy ze centrala wlaczala sie mimo ze byla na off, rozwiazanie Błąd eepromu przez co odczytywalismy jakies smieci i nam sie nie inizjalizowala centrala przez co mielismy na 0 a to znaczy start |
 
| [amk7_unicentral-v137-c952-f37cb17-20181024.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v137-c952-f37cb17-20181024.bl2) | 20181024-14:45:49 | [f37cb176a8e929e816ba7bca528034fff700aa96](https://bitbucket.org/amk_rd/amk7_unicentral/commits/f37cb176a8e929e816ba7bca528034fff700aa96) | w przypadku gdy byla awaria wylaczala nagrzewnice a w trybie zimowym ma nie wylaczac |
 
| [amk7_unicentral-v138-c960-2d97aff-20181025.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v138-c960-2d97aff-20181025.bl2) | 20181025-09:20:00 | [2d97aff5fae8270edc7102feb3a9f7eb85adf45a](https://bitbucket.org/amk_rd/amk7_unicentral/commits/2d97aff5fae8270edc7102feb3a9f7eb85adf45a) | wyrzuciłem nieobslugiwane rejestry |
 
| [amk7_unicentral-v139-c963-745e150-20181026.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v139-c963-745e150-20181026.bl2) | 20181026-09:09:35 | [745e150df8ef7d2d4335064e1c69f57d52ebfbdc](https://bitbucket.org/amk_rd/amk7_unicentral/commits/745e150df8ef7d2d4335064e1c69f57d52ebfbdc) | dodanie wymiennika glikolowego |
 
| [amk7_unicentral-v140-c973-ac1e91b-20181030.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v140-c973-ac1e91b-20181030.bl2) | 20181030-09:55:38 | [ac1e91ba2c1793343f6dcfa8e52101747dd97ec1](https://bitbucket.org/amk_rd/amk7_unicentral/commits/ac1e91ba2c1793343f6dcfa8e52101747dd97ec1) | obsluga panelu okraglego |
 
| [amk7_unicentral-v141-c978-ce98ba3-20181030.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v141-c978-ce98ba3-20181030.bl2) | 20181030-10:44:57 | [ce98ba360e82ffc8978be152a236cf61aef8a686](https://bitbucket.org/amk_rd/amk7_unicentral/commits/ce98ba360e82ffc8978be152a236cf61aef8a686) | panel z biegami dziala |
 
| [amk7_unicentral-v142-c980-4a66919-20181030.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v142-c980-4a66919-20181030.bl2) | 20181030-15:02:12 | [4a66919e249f408d8e2683548b3bef1310e793a5](https://bitbucket.org/amk_rd/amk7_unicentral/commits/4a66919e249f408d8e2683548b3bef1310e793a5) | dodalem opoznienie w odczycie temp nawiewu na 5s, Tasma klejaca na zaklocenia |
 
| [amk7_unicentral-v143-c981-e355258-20181030.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v143-c981-e355258-20181030.bl2) | 20181030-15:07:52 | [e3552589f4bed80758687f246510f709b43e7425](https://bitbucket.org/amk_rd/amk7_unicentral/commits/e3552589f4bed80758687f246510f709b43e7425) | zmiana nazwy timera odliczajacego blad na nawiewie |
 
| [amk7_unicentral-v144-c983-0b5c350-20181031.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v144-c983-0b5c350-20181031.bl2) | 20181031-09:07:17 | [0b5c350ded1525f8e1a97ad357e5dfa162a64f9d](https://bitbucket.org/amk_rd/amk7_unicentral/commits/0b5c350ded1525f8e1a97ad357e5dfa162a64f9d) | dodalem opoznienie w braku czujnika nawiewu |
 
| [amk7_unicentral-v145-c984-9b2b261-20181031.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v145-c984-9b2b261-20181031.bl2) | 20181031-11:05:59 | [9b2b2611617565da7eec74b11a43a85e8f292259](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9b2b2611617565da7eec74b11a43a85e8f292259) | centrala nie blokowala sie po 500h gdy byla w trybie czasowym |
 
| [amk7_unicentral-v147-c991-552873e-20181105.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v147-c991-552873e-20181105.bl2) | 20181105-14:28:31 | [552873e9dbc2212142654dd13aff0951c9eed27c](https://bitbucket.org/amk_rd/amk7_unicentral/commits/552873e9dbc2212142654dd13aff0951c9eed27c) | przy czujniku 0-10 wykrywamy brak czujnika i sie nam przelacza samo |
 
| [amk7_unicentral-v148-c100-fe13c72-20181107.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v148-c100-fe13c72-20181107.bl2) | 20181107-15:00:17 | [fe13c72f8967f2053e24f1894cd334df20b8b318](https://bitbucket.org/amk_rd/amk7_unicentral/commits/fe13c72f8967f2053e24f1894cd334df20b8b318) | stworzylem 3 slot z triakow i przypisalem do recyrkulacji i nagrzewnicy elektrycznej musze dodac jeszcze do wentylatorw  jako start dla falownikow |
 
| [amk7_unicentral-v149-c100-a561be2-20181107.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v149-c100-a561be2-20181107.bl2) | 20181107-15:53:08 | [a561be292963e0bf63e74a1d4361c0c1322e1480](https://bitbucket.org/amk_rd/amk7_unicentral/commits/a561be292963e0bf63e74a1d4361c0c1322e1480) | wentylatory 0-10 ze startem falownikow na slot1,2,3 |
 
| [amk7_unicentral-v150-c100-70a5d4e-20181108.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v150-c100-70a5d4e-20181108.bl2) | 20181108-10:06:57 | [70a5d4e2530646dbefba15502eee5695a6684d15](https://bitbucket.org/amk_rd/amk7_unicentral/commits/70a5d4e2530646dbefba15502eee5695a6684d15) | dodalem w nagrzewnicy rodzaj silownika 0-10 lub 2-10 |
 
| [amk7_unicentral-v151-c100-a6482e2-20181108.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v151-c100-a6482e2-20181108.bl2) | 20181108-11:19:28 | [a6482e2b7f854a8d87d108cbdc09355b6115b665](https://bitbucket.org/amk_rd/amk7_unicentral/commits/a6482e2b7f854a8d87d108cbdc09355b6115b665) | podawalem wskaznik zmiennej lokalnej gdy odwolywalem sie do niej to jako ze to byla zmienna lokalna to w pamieci juz nie bylo jej pobieralem wartosc z dupy |
 
| [amk7_unicentral-v152-c100-82e0876-20181108.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v152-c100-82e0876-20181108.bl2) | 20181108-13:02:39 | [82e0876fe1d4c35dfd71b8db11e2539384b8b440](https://bitbucket.org/amk_rd/amk7_unicentral/commits/82e0876fe1d4c35dfd71b8db11e2539384b8b440) | dodalem przesuniecie naw i wyw dla stalego przeplywu |
 
| [amk7_unicentral-v153-c101-6354cd7-20181110.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v153-c101-6354cd7-20181110.bl2) | 20181110-11:57:49 | [6354cd75e092278f4f9fb9235bb528fafee6637b](https://bitbucket.org/amk_rd/amk7_unicentral/commits/6354cd75e092278f4f9fb9235bb528fafee6637b) | przy wylaczonej centrali nie dziala wymiennik glikolowy |
 
| [amk7_unicentral-v154-c103-c3594c7-20181114.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v154-c103-c3594c7-20181114.bl2) | 20181114-10:03:24 | [c3594c71d3375f8f86a26bb65247ce6f0fb98545](https://bitbucket.org/amk_rd/amk7_unicentral/commits/c3594c71d3375f8f86a26bb65247ce6f0fb98545) | merge ok |
 
| [amk7_unicentral-v155-c104-02d505e-20181116.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v155-c104-02d505e-20181116.bl2) | 20181116-11:01:14 | [02d505ecb6ac27e4e21f57290193a6ef88212bd0](https://bitbucket.org/amk_rd/amk7_unicentral/commits/02d505ecb6ac27e4e21f57290193a6ef88212bd0) | teraz po wlaczeniu z panelu na ON gdy centrala bedzie w alarmie to zaktualizuje na panelu na OFF |
 
| [amk7_unicentral-v156-c105-be798e2-20181119.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v156-c105-be798e2-20181119.bl2) | 20181119-11:07:50 | [be798e2ed905c52b296b038f67c9e4136dff1a08](https://bitbucket.org/amk_rd/amk7_unicentral/commits/be798e2ed905c52b296b038f67c9e4136dff1a08) | poprawilem panel i dodanie wymienika wł wył  recyrkulacja od Co2 |
 
| [amk7_unicentral-v157-c106-64512fb-20181126.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v157-c106-64512fb-20181126.bl2) | 20181126-08:28:21 | [64512fbdab149411234b15541cf1563291811f0a](https://bitbucket.org/amk_rd/amk7_unicentral/commits/64512fbdab149411234b15541cf1563291811f0a) | znalazlem blad w nagrzewnicy trojpunktowej, temp zadana nie byla procesu a poprostu zadana |
 
| [amk7_unicentral-v158-c107-8dc86d4-20181128.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v158-c107-8dc86d4-20181128.bl2) | 20181128-14:29:57 | [8dc86d4ef2c448231c7b1b1b941900af3ccd9206](https://bitbucket.org/amk_rd/amk7_unicentral/commits/8dc86d4ef2c448231c7b1b1b941900af3ccd9206) | wyswietlanie warningu bez wplywu na alarmy krytyczne |
 
| [amk7_unicentral-v159-c107-e49dba5-20181128.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v159-c107-e49dba5-20181128.bl2) | 20181128-15:36:00 | [e49dba5e0c8712442dde229ba990dc01d1212771](https://bitbucket.org/amk_rd/amk7_unicentral/commits/e49dba5e0c8712442dde229ba990dc01d1212771) | do przesuniecia przy stalym przeplywie dodalem signed int zeby mozna bylo dodawac i odejmowac od zadanej wartosci |
 
| [amk7_unicentral-v160-c107-b75da28-20181130.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v160-c107-b75da28-20181130.bl2) | 20181130-13:06:22 | [b75da281799ea7d4c12e58ca09ce0f7b2a3bea0a](https://bitbucket.org/amk_rd/amk7_unicentral/commits/b75da281799ea7d4c12e58ca09ce0f7b2a3bea0a) | musi byc 3 bo jak 1 to nie zapisuje do flesha |
 
| [amk7_unicentral-v161-c107-d7ec343-20181212.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v161-c107-d7ec343-20181212.bl2) | 20181212-08:42:11 | [d7ec343730dbbed5b6bc54ee0935e3636403d2f0](https://bitbucket.org/amk_rd/amk7_unicentral/commits/d7ec343730dbbed5b6bc54ee0935e3636403d2f0) | moze cos pomoze z lepsza pracą BMS |
 
| [amk7_unicentral-v162-c108-7d3951c-20181212.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v162-c108-7d3951c-20181212.bl2) | 20181212-09:04:08 | [7d3951c679ce8412ce74c6a1aaced92d4297944d](https://bitbucket.org/amk_rd/amk7_unicentral/commits/7d3951c679ce8412ce74c6a1aaced92d4297944d) | zglaszaj za niska temp nawiewu jesli centrala pracuje powyzej 35s |
 
| [amk7_unicentral-v163-c108-dd97e24-20181213.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v163-c108-dd97e24-20181213.bl2) | 20181213-13:55:56 | [dd97e248b53c36e38d31296070418719213904bc](https://bitbucket.org/amk_rd/amk7_unicentral/commits/dd97e248b53c36e38d31296070418719213904bc) | znalazlem Bug odnosnie temp nawiewu, nie by to signed i jak temp jest ujemna to wykywalo ze jest 65tys stopni i wyrzucalo awarie krytycznie wysoka temp nawiewu |
 
| [amk7_unicentral-v164-c109-76b7f53-20181213.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v164-c109-76b7f53-20181213.bl2) | 20181213-16:10:34 | [76b7f53d5d3414eac00ec8008d83f553ebfd5940](https://bitbucket.org/amk_rd/amk7_unicentral/commits/76b7f53d5d3414eac00ec8008d83f553ebfd5940) | Bug brak jednakowego rzutowania dla dwóch porównywanych wartości |
 
| [amk7_unicentral-v165-c109-acbc13e-20190107.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v165-c109-acbc13e-20190107.bl2) | 20190107-09:46:31 | [acbc13ef6c306464c759dab1925866107acbb42d](https://bitbucket.org/amk_rd/amk7_unicentral/commits/acbc13ef6c306464c759dab1925866107acbb42d) | poprawiam modbus slave |
 
| [amk7_unicentral-v166-c109-0bee747-20190108.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v166-c109-0bee747-20190108.bl2) | 20190108-15:13:42 | [0bee747fb7c95c3e4518fc0e567703d4ee4c35dd](https://bitbucket.org/amk_rd/amk7_unicentral/commits/0bee747fb7c95c3e4518fc0e567703d4ee4c35dd) | centrala zablokowana a algorytm dzialal xd, nie wiem jakim cudem to sie ostalo kiedys juz to poprawialem |
 
| [amk7_unicentral-v167-c111-86a222b-20190118.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v167-c111-86a222b-20190118.bl2) | 20190118-07:53:15 | [86a222b4f4e44cfba6447b8812821be8f33319f9](https://bitbucket.org/amk_rd/amk7_unicentral/commits/86a222b4f4e44cfba6447b8812821be8f33319f9) | Merge branch 'cleanModbusSlave'

# Conflicts:
#	pic/dist/default/production/pic.production.bl2
#	pic/dist/default/production/pic.production.elf
#	pic/dist/default/production/pic.production.hex
#	pic/dist/default/production/pic.production.map
#	pic/ezbl_integration/ezbl_dual_partition.mk |
 
| [amk7_unicentral-v168-c112-335b011-20190118.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v168-c112-335b011-20190118.bl2) | 20190118-14:18:51 | [335b011a61f599ca435401b0060eee8fd386723a](https://bitbucket.org/amk_rd/amk7_unicentral/commits/335b011a61f599ca435401b0060eee8fd386723a) | stalu pzeplyw i cisnienie zrobone musze teraz spr czy jest modbusSlave sensor pressure |
 
| [amk7_unicentral-v169-c113-9ad8783-20190121.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v169-c113-9ad8783-20190121.bl2) | 20190121-13:17:37 | [9ad87830f4a22c997c46c2ba5a6f8c67c5c2aa62](https://bitbucket.org/amk_rd/amk7_unicentral/commits/9ad87830f4a22c997c46c2ba5a6f8c67c5c2aa62) | zrobilem wstepna nagrzewnice bede testowal |
 
| [amk7_unicentral-v170-c116-962f4e4-20190121.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v170-c116-962f4e4-20190121.bl2) | 20190121-16:21:55 | [962f4e476cc800bef19e4670f2c17c3c89f27823](https://bitbucket.org/amk_rd/amk7_unicentral/commits/962f4e476cc800bef19e4670f2c17c3c89f27823) | dziala ale nie zmniejsza ponizej badz rowno 30 |
 
| [amk7_unicentral-v171-c117-1646349-20190122.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v171-c117-1646349-20190122.bl2) | 20190122-14:18:29 | [1646349f9ffcf9cb76ca134365cf2f912e1837ef](https://bitbucket.org/amk_rd/amk7_unicentral/commits/1646349f9ffcf9cb76ca134365cf2f912e1837ef) | zrobilem nagrzewnice wstepną bede musial jeszcze dodac opcje przypisywania wydajnosci w przypadku jesli jest cos innego niz wydajnosc zadana |
 
| [amk7_unicentral-v172-c117-f949ed0-20190122.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v172-c117-f949ed0-20190122.bl2) | 20190122-15:51:00 | [f949ed0c2cd262578ede98f9eabfc6b2280b9154](https://bitbucket.org/amk_rd/amk7_unicentral/commits/f949ed0c2cd262578ede98f9eabfc6b2280b9154) | do nastaw uzytkownika dodalem tryby |
 
| [amk7_unicentral-v173-c118-394a117-20190123.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v173-c118-394a117-20190123.bl2) | 20190123-16:12:14 | [394a117a5517520e7718fec12d6a33bf3823fca2](https://bitbucket.org/amk_rd/amk7_unicentral/commits/394a117a5517520e7718fec12d6a33bf3823fca2) | zrobilem godz nocne dla reg liniowego i znalazlem bug wyswietlal sie blad czujnika cisnienia a go nie wykorzystywalem |
 
| [amk7_unicentral-v174-c118-c33fc61-20190124.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v174-c118-c33fc61-20190124.bl2) | 20190124-11:58:03 | [c33fc61dc3e682d53415e100e1320201d32ea75b](https://bitbucket.org/amk_rd/amk7_unicentral/commits/c33fc61dc3e682d53415e100e1320201d32ea75b) | poprawilem alarmy |
 
| [amk7_unicentral-v175-c118-14f9353-20190131.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v175-c118-14f9353-20190131.bl2) | 20190131-13:16:57 | [14f93539df67ffe49519cced6219ec8436f1a741](https://bitbucket.org/amk_rd/amk7_unicentral/commits/14f93539df67ffe49519cced6219ec8436f1a741) | wartosc defaultowa byla niepoprawna |
 
| [amk7_unicentral-v176-c119-39ff1a7-20190205.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v176-c119-39ff1a7-20190205.bl2) | 20190205-09:17:48 | [39ff1a7ad08c8d74b210b4b4b1a3a9942ae7734a](https://bitbucket.org/amk_rd/amk7_unicentral/commits/39ff1a7ad08c8d74b210b4b4b1a3a9942ae7734a) | centrala wylaczona na stalem w trybie czasowy |
 
| [amk7_unicentral-v177-c119-54c0a81-20190205.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_unicentral/production/amk7_unicentral-v177-c119-54c0a81-20190205.bl2) | 20190205-10:28:58 | [54c0a8123ea8cdaa6bab72e51fe05370c7465235](https://bitbucket.org/amk_rd/amk7_unicentral/commits/54c0a8123ea8cdaa6bab72e51fe05370c7465235) | naprawiłem harmonogram |
 
