 
##amk7_uniheatpump##
|NAZWA PLIKU 					|	DATA			| COMMIT Z KTOREGO POCHODZI KOMPILACJA		| OPIS COMMITU					|
|---|---|---|---|
| [amk7_uniheatpump-v8-c291-75a4506-20180720.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v8-c291-75a4506-20180720.bl2) | 20180720-14:32:15 | [75a45063b5935c394040d86b49c5b1d50a9e35c0](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/75a45063b5935c394040d86b49c5b1d50a9e35c0) | bitbucket-pipelines.yml edited online with Bitbucket |
 
| [amk7_uniheatpump-v9-c300-c4267cc-20180726.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v9-c300-c4267cc-20180726.bl2) | 20180726-10:19:02 | [c4267cc530e83eee0a5cb4f348f0737384d9c35f](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/c4267cc530e83eee0a5cb4f348f0737384d9c35f) | obliczanie CRC w LNS, nie wiem czy dobrze się oblicza trzeba bedzie przetestowac |
 
| [amk7_uniheatpump-v10-c301-59612a2-20180730.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v10-c301-59612a2-20180730.bl2) | 20180730-11:11:36 | [59612a24c60479ddd24bf9ccd3a10ca8830621fd](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/59612a24c60479ddd24bf9ccd3a10ca8830621fd) | zmiejszylem timeouty i jedna flage wyrzucilem |
 
| [amk7_uniheatpump-v11-c308-db17d6c-20180730.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v11-c308-db17d6c-20180730.bl2) | 20180730-11:48:56 | [db17d6c7dfb01764fe1ead7f7768a4e4a46bd52f](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/db17d6c7dfb01764fe1ead7f7768a4e4a46bd52f) | wywalilem bootloader w jednym programie teraz jest przelaczanie sie na inna partycje |
 
| [amk7_uniheatpump-v12-c309-6929ce0-20180731.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v12-c309-6929ce0-20180731.bl2) | 20180731-07:40:58 | [6929ce0cfbf522dca832e782d81435c3d6da562c](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/6929ce0cfbf522dca832e782d81435c3d6da562c) | bede debugował program |
 
| [amk7_uniheatpump-v13-c313-36ea21e-20180731.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v13-c313-36ea21e-20180731.bl2) | 20180731-10:46:22 | [36ea21e705e9aed7d25ef26ea451f1bed63dc572](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/36ea21e705e9aed7d25ef26ea451f1bed63dc572) | zmiany u mateusza |
 
| [amk7_uniheatpump-v14-c314-dc002a3-20180731.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v14-c314-dc002a3-20180731.bl2) | 20180731-11:03:17 | [dc002a324b21590b2e292db738733b11c9c8527f](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/dc002a324b21590b2e292db738733b11c9c8527f) | kolejna zmiana u  mateusza |
 
| [amk7_uniheatpump-v16-c323-5c71d10-20180810.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v16-c323-5c71d10-20180810.bl2) | 20180810-09:36:39 | [5c71d10ccb49fa36ad79010cbf37dbb72d76b6f9](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/5c71d10ccb49fa36ad79010cbf37dbb72d76b6f9) | wawa - zamiana na triaka |
 
| [amk7_uniheatpump-v17-c331-72280ed-20180810.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v17-c331-72280ed-20180810.bl2) | 20180810-12:12:13 | [72280ed253f6208d927bb5b93d0e4230c56ef140](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/72280ed253f6208d927bb5b93d0e4230c56ef140) | zmienilem pk2 z pk1, pompa sie uruchamia i grzeje trak tez. brak uszczelki wiec koniec pracy |
 
| [amk7_uniheatpump-v18-c336-f77e2fa-20181003.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v18-c336-f77e2fa-20181003.bl2) | 20181003-16:27:28 | [f77e2faa1648573d9af010b6c5e883da84993ec6](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/f77e2faa1648573d9af010b6c5e883da84993ec6) | wyrzucilem BRIDGE |
 
| [amk7_uniheatpump-v19-c340-6c7203c-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v19-c340-6c7203c-20181018.bl2) | 20181018-13:04:27 | [6c7203cfdd573332b0ecc1456c2184633a16bb87](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/6c7203cfdd573332b0ecc1456c2184633a16bb87) | dodanie kolejnej jednostki LNS z nowym protokołem |
 
| [amk7_uniheatpump-v20-c341-394ec0a-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v20-c341-394ec0a-20181018.bl2) | 20181018-14:26:40 | [394ec0a54883b1078bd4a2a511190fa964166df4](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/394ec0a54883b1078bd4a2a511190fa964166df4) | na sztywno nowy lns |
 
| [amk7_uniheatpump-v21-c350-aa92ede-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v21-c350-aa92ede-20181018.bl2) | 20181018-17:08:10 | [aa92ede701ff729265b5d2d4305ffc33d188bcbf](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/aa92ede701ff729265b5d2d4305ffc33d188bcbf) | jest jakas komunikacja testujemy na dole |
 
| [amk7_uniheatpump-v22-c351-6124f4c-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v22-c351-6124f4c-20181018.bl2) | 20181018-17:24:41 | [6124f4cfbaa3e2eac2b34dfed2639fa6fdc0d4b9](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/6124f4cfbaa3e2eac2b34dfed2639fa6fdc0d4b9) | zapomnialem odwrocic odbioru |
 
| [amk7_uniheatpump-v23-c352-ebb2d02-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v23-c352-ebb2d02-20181018.bl2) | 20181018-17:35:59 | [ebb2d0282487d50f079b6242a7ac422df52eba8b](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/ebb2d0282487d50f079b6242a7ac422df52eba8b) | zmienilem na 8e1 |
 
| [amk7_uniheatpump-v24-c353-686cc26-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v24-c353-686cc26-20181018.bl2) | 20181018-18:21:00 | [686cc2609b6b371ebf424c9761becc0e1e50d310](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/686cc2609b6b371ebf424c9761becc0e1e50d310) | bardziej zabezpieczylem inicjalizacje |
 
| [amk7_uniheatpump-v25-c354-fa69610-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v25-c354-fa69610-20181018.bl2) | 20181018-18:34:47 | [fa69610c7af67b5711b2efa2fae99165eb259433](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/fa69610c7af67b5711b2efa2fae99165eb259433) | zmienilem na 8e2 |
 
| [amk7_uniheatpump-v26-c355-0faa905-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v26-c355-0faa905-20181018.bl2) | 20181018-18:45:17 | [0faa905ac31aa089f2b5257d1077bb8f2d8b1f9e](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/0faa905ac31aa089f2b5257d1077bb8f2d8b1f9e) | odwrocilem tx i rx |
 
| [amk7_uniheatpump-v27-c356-928477d-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v27-c356-928477d-20181018.bl2) | 20181018-18:51:12 | [928477d41e283dc8f2dd06c56a5ee88ef7ae46c4](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/928477d41e283dc8f2dd06c56a5ee88ef7ae46c4) | powrocilem do tego co bylo |
 
| [amk7_uniheatpump-v28-c357-fac0147-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v28-c357-fac0147-20181018.bl2) | 20181018-18:59:38 | [fac014721e0370ea33b8336d7f834a19c4594e14](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/fac014721e0370ea33b8336d7f834a19c4594e14) | dodalem licznik odebranych danych |
 
| [amk7_uniheatpump-v29-c359-84c492b-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v29-c359-84c492b-20181018.bl2) | 20181018-19:25:21 | [84c492b370dcb89543d01b4fa7f27687b7ff59ee](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/84c492b370dcb89543d01b4fa7f27687b7ff59ee) | reczne sterowanie |
 
| [amk7_uniheatpump-v30-c365-c9693a8-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v30-c365-c9693a8-20181018.bl2) | 20181018-19:41:22 | [c9693a87db97cedb9dbdc2dd50df2f555b44ee3c](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/c9693a87db97cedb9dbdc2dd50df2f555b44ee3c) | zwiekszylem timeout |
 
| [amk7_uniheatpump-v31-c366-0d7ee89-20181018.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v31-c366-0d7ee89-20181018.bl2) | 20181018-20:06:31 | [0d7ee892a77b3bf1040c473e8a5aaad9400afee4](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/0d7ee892a77b3bf1040c473e8a5aaad9400afee4) | zmodyfikowanie stopu |
 
| [amk7_uniheatpump-v32-c371-4645953-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v32-c371-4645953-20181019.bl2) | 20181019-09:17:43 | [464595389c7ae734334056e30f3f4a075d368ab0](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/464595389c7ae734334056e30f3f4a075d368ab0) | dodanie serwisowego zalaczania pompami |
 
| [amk7_uniheatpump-v33-c372-ff190e4-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v33-c372-ff190e4-20181019.bl2) | 20181019-10:13:34 | [ff190e416f4ae32302b8ce37a925ebd302d1eedf](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/ff190e416f4ae32302b8ce37a925ebd302d1eedf) | 2 jednostki midea |
 
| [amk7_uniheatpump-v34-c374-17d57ce-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v34-c374-17d57ce-20181019.bl2) | 20181019-10:22:15 | [17d57cef95013781103f246360cb9d9aedfa3a1c](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/17d57cef95013781103f246360cb9d9aedfa3a1c) | tryb serwisowy midea 2 |
 
| [amk7_uniheatpump-v35-c375-7a5c789-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v35-c375-7a5c789-20181019.bl2) | 20181019-11:13:38 | [7a5c78905a020e155341cf23644e3245bbbb2279](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/7a5c78905a020e155341cf23644e3245bbbb2279) | dodanie zmiennej ktora informuje stan 3drogowego dla cwu i co |
 
| [amk7_uniheatpump-v36-c377-1630885-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v36-c377-1630885-20181019.bl2) | 20181019-12:07:18 | [16308857cb4a0db6ff2271c8aa0c25d880a41ee2](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/16308857cb4a0db6ff2271c8aa0c25d880a41ee2) | zamiana przekaznikow z 5 na 4 |
 
| [amk7_uniheatpump-v37-c378-393cac4-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v37-c378-393cac4-20181019.bl2) | 20181019-12:50:08 | [393cac4a59320d954dbc962428bd2757119ffa6e](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/393cac4a59320d954dbc962428bd2757119ffa6e) | grzejemy jedna pompa |
 
| [amk7_uniheatpump-v38-c379-aa7c978-20181019.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v38-c379-aa7c978-20181019.bl2) | 20181019-13:51:10 | [aa7c978e552b5561f7a53f61cbe21ac12c1d1317](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/aa7c978e552b5561f7a53f61cbe21ac12c1d1317) | sprawdzamy errory |
 
| [amk7_uniheatpump-v39-c382-6d23f65-20181105.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v39-c382-6d23f65-20181105.bl2) | 20181105-15:49:43 | [6d23f65f17dddeb5b1dafd44e916376fbbb62ab8](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/6d23f65f17dddeb5b1dafd44e916376fbbb62ab8) | dodalem WE1 na CWU i WE2 na CO, na sztywno nastawy pompy |
 
| [amk7_uniheatpump-v40-c385-26231f1-20181123.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v40-c385-26231f1-20181123.bl2) | 20181123-14:22:35 | [26231f14160034d0f3704941175d62d55f921447](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/26231f14160034d0f3704941175d62d55f921447) | przedluzenie pracy pompy obiegowej, dodanie apstract dla pompy, jezeli na freon powrot lub zasilanie  temp spadanie ponizej 5stopni wlaczy pompke obiegowa |
 
| [amk7_uniheatpump-v41-c392-87fed63-20190128.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v41-c392-87fed63-20190128.bl2) | 20190128-14:04:53 | [87fed6327da3659e3a7bdf8efaf6e234ebb318c8](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/87fed6327da3659e3a7bdf8efaf6e234ebb318c8) | dodalem wpisywanie wartosci do ramek |
 
| [amk7_uniheatpump-v42-c406-f663dc3-20190129.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v42-c406-f663dc3-20190129.bl2) | 20190129-12:25:50 | [f663dc38273dede11e64a26344923b69a4d25841](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/f663dc38273dede11e64a26344923b69a4d25841) | dalem lepszego modbusa, wartosci wpisywane jako temp zostaly przeskalowane |
 
| [amk7_uniheatpump-v43-c419-3457253-20190130.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v43-c419-3457253-20190130.bl2) | 20190130-16:03:16 | [34572536cb8e36564ee1a5cef6a80884073d35ab](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/34572536cb8e36564ee1a5cef6a80884073d35ab) | nie ma inicjalizacji , poprawilem wysylana wartosc temperatury |
 
| [amk7_uniheatpump-v44-c436-bd1b095-20190131.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v44-c436-bd1b095-20190131.bl2) | 20190131-16:08:04 | [bd1b095e56659561946a9b478664e0e5a795a491](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/bd1b095e56659561946a9b478664e0e5a795a491) | poprawki |
 
| [amk7_uniheatpump-v45-c438-f33fd1e-20190201.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v45-c438-f33fd1e-20190201.bl2) | 20190201-09:39:44 | [f33fd1e1cf894f5acea378c94ea63ea437c71ed0](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/f33fd1e1cf894f5acea378c94ea63ea437c71ed0) | wgrywam na lyski |
 
| [amk7_uniheatpump-v46-c441-84d3c8e-20190201.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v46-c441-84d3c8e-20190201.bl2) | 20190201-14:17:36 | [84d3c8ecee52d99dd671aa9847779961932b667f](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/84d3c8ecee52d99dd671aa9847779961932b667f) | dodałem licznik energi |
 
| [amk7_uniheatpump-v47-c442-5f5b3ae-20190201.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v47-c442-5f5b3ae-20190201.bl2) | 20190201-14:26:58 | [5f5b3aeb09c5e8d293b30b855d542cabfd843fef](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/5f5b3aeb09c5e8d293b30b855d542cabfd843fef) | poprawilem licznik |
 
| [amk7_uniheatpump-v48-c445-3c28cab-20190205.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v48-c445-3c28cab-20190205.bl2) | 20190205-15:11:38 | [3c28cab0647f775e4a5af823fa3f856c05f4b310](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/3c28cab0647f775e4a5af823fa3f856c05f4b310) | sterowanie 2 sprezarkami |
 
| [amk7_uniheatpump-v49-c456-fcd62b7-20190206.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v49-c456-fcd62b7-20190206.bl2) | 20190206-13:39:16 | [fcd62b793e6974b3d814548afadb763c75ec334c](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/fcd62b793e6974b3d814548afadb763c75ec334c) | zrobilem dzielenie na ilosc dzialajacych pomp |
 
| [amk7_uniheatpump-v50-c458-10f4e2a-20190206.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v50-c458-10f4e2a-20190206.bl2) | 20190206-13:53:20 | [10f4e2a2382a40977b5867315ae9b92cce2ec501](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/10f4e2a2382a40977b5867315ae9b92cce2ec501) | nie do tego projektu wrzucilem zliczanie uruchomien sprezarek |
 
| [amk7_uniheatpump-v51-c460-c61da95-20190206.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v51-c460-c61da95-20190206.bl2) | 20190206-14:56:41 | [c61da95f6c074a2f9cc2c71f8fb9425cf5c3554b](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/c61da95f6c074a2f9cc2c71f8fb9425cf5c3554b) | nie wylaczalo pompy i za duzo liczylo uruchomionych cykli, dalem 10s na wysylanie grzania po wlaczeniu przekaznika |
 
| [amk7_uniheatpump-v52-c462-43d5ba0-20190206.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v52-c462-43d5ba0-20190206.bl2) | 20190206-15:48:19 | [43d5ba0437f4a61a033d937ddb99621d2ce14839](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/43d5ba0437f4a61a033d937ddb99621d2ce14839) | poprawilem zliczanie dzialania central i czas uruchomenia pompy |
 
| [amk7_uniheatpump-v53-c463-ed2dbb8-20190206.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v53-c463-ed2dbb8-20190206.bl2) | 20190206-16:13:51 | [ed2dbb8eb4bc97de943177a15861d1690ff7af39](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/ed2dbb8eb4bc97de943177a15861d1690ff7af39) | break mi uciekl |
 
| [amk7_uniheatpump-v54-c464-52c6110-20190206.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v54-c464-52c6110-20190206.bl2) | 20190206-16:54:13 | [52c6110ce98134db8408a2f9e60f738d35326040](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/52c6110ce98134db8408a2f9e60f738d35326040) | sprawdzam co sie dzielje na odbiorze w drugiej pompie |
 
| [amk7_uniheatpump-v55-c465-e174d33-20190207.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v55-c465-e174d33-20190207.bl2) | 20190207-08:24:22 | [e174d33f5a948f28ceeb30f1883ac59f1323c9b5](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/e174d33f5a948f28ceeb30f1883ac59f1323c9b5) | bede spr ile otrzymuje bajtow |
 
| [amk7_uniheatpump-v56-c468-b0bdcd9-20190207.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v56-c468-b0bdcd9-20190207.bl2) | 20190207-08:59:18 | [b0bdcd9f451cc52ea996934f97ce55912f3d1cc3](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/b0bdcd9f451cc52ea996934f97ce55912f3d1cc3) | wpisuje wartosci do rejestrow |
 
| [amk7_uniheatpump-v57-c472-dc84924-20190207.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v57-c472-dc84924-20190207.bl2) | 20190207-09:49:12 | [dc849243d29fb66c1c76ac98db310be60c71469e](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/dc849243d29fb66c1c76ac98db310be60c71469e) | wyrzucile jedna czesc z dodawania do rejestrow |
 
| [amk7_uniheatpump-v58-c473-13a357f-20190207.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v58-c473-13a357f-20190207.bl2) | 20190207-10:12:10 | [13a357ff9f04c42b73bdbeaab9ab9f897150cb81](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/13a357ff9f04c42b73bdbeaab9ab9f897150cb81) | jakas drama |
 
| [amk7_uniheatpump-v59-c474-eaf11c2-20190207.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v59-c474-eaf11c2-20190207.bl2) | 20190207-10:49:23 | [eaf11c23784bbc34717f0441c63c1c2a5c28a324](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/eaf11c23784bbc34717f0441c63c1c2a5c28a324) | fizyczne stany pomy ciepla odczytuje z ramek odbiorczych |
 
| [amk7_uniheatpump-v60-c475-2ebbb02-20190207.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v60-c475-2ebbb02-20190207.bl2) | 20190207-10:54:42 | [2ebbb02736e3b01ccf7c1c4b63dd7a745d8d3769](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/2ebbb02736e3b01ccf7c1c4b63dd7a745d8d3769) | miałem zajety juz ten rejestr |
 
| [amk7_uniheatpump-v61-c492-867fef1-20190211.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v61-c492-867fef1-20190211.bl2) | 20190211-15:12:58 | [867fef11a24ede6010d17d4d5d13e47425d22644](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/867fef11a24ede6010d17d4d5d13e47425d22644) | zrobilem sterowanie mocą wgrywam na pompe |
 
| [amk7_uniheatpump-v62-c494-6b7fe06-20190211.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v62-c494-6b7fe06-20190211.bl2) | 20190211-16:13:41 | [6b7fe06b8f9e061c9095e2e5fb559bf1123fe2b1](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/6b7fe06b8f9e061c9095e2e5fb559bf1123fe2b1) | wpisywalem za kazdym razem to samo |
 
| [amk7_uniheatpump-v63-c495-2d25ba9-20190211.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v63-c495-2d25ba9-20190211.bl2) | 20190211-16:30:40 | [2d25ba989c8640d306d7edc502878eaa1ad8d33f](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/2d25ba989c8640d306d7edc502878eaa1ad8d33f) | przypisywalem zly stan midea 2 |
 
| [amk7_uniheatpump-v64-c497-5537109-20190211.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v64-c497-5537109-20190211.bl2) | 20190211-17:10:13 | [553710909d266fbe073eae90b11a09f6902bd871](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/553710909d266fbe073eae90b11a09f6902bd871) | ograniczylem bo nie dziala |
 
| [amk7_uniheatpump-v65-c498-83533a0-20190212.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v65-c498-83533a0-20190212.bl2) | 20190212-08:15:54 | [83533a05d163d52557590103a46395e42c425502](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/83533a05d163d52557590103a46395e42c425502) | debuguje |
 
| [amk7_uniheatpump-v66-c499-1b6f5e2-20190212.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v66-c499-1b6f5e2-20190212.bl2) | 20190212-08:39:02 | [1b6f5e27db291a889e954390c15a43455cd93a47](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/1b6f5e27db291a889e954390c15a43455cd93a47) | cos wartosci sie nie przypisuja do regulatora histerezowego |
 
| [amk7_uniheatpump-v67-c500-f78a0a0-20190212.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v67-c500-f78a0a0-20190212.bl2) | 20190212-09:04:42 | [f78a0a0e23676d0bef735cfdb7ce60f1fb745d18](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/f78a0a0e23676d0bef735cfdb7ce60f1fb745d18) | jest okej |
 
| [amk7_uniheatpump-v68-c502-df5d775-20190212.bl2](https://bitbucket.org/amk_rd/amk/raw/master/firmware/amk7_uniheatpump/production/amk7_uniheatpump-v68-c502-df5d775-20190212.bl2) | 20190212-14:30:41 | [df5d775e8fae0ff20bf68ce8f040be041b8d285b](https://bitbucket.org/amk_rd/amk7_uniheatpump/commits/df5d775e8fae0ff20bf68ce8f040be041b8d285b) | zmniejszylem hist i wartosc odejmowana |
 
